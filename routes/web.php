<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



Auth::routes();

Route::get('/', 'HomeController@index');

Route::group(['middleware' => 'auth'], function () {
    Route::resource('influencers', 'InfluencerController');

    Route::resource('collections', 'CollectionController');
});


Route::any('collection-create', 'CollectionController@collection_create');
Route::any('collection-update', 'CollectionController@collection_update');
Route::any('collection-delete', 'CollectionController@collection_delete');














