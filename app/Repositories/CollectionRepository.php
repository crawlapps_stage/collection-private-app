<?php

namespace App\Repositories;

use App\Models\Collection;
use App\Repositories\BaseRepository;

/**
 * Class CollectionRepository
 * @package App\Repositories
 * @version August 26, 2019, 7:10 am UTC
*/

class CollectionRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'name',
        'english_title',
        'arabic_title',
        'english_image',
        'arabic_image'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Collection::class;
    }
}
