<?php

namespace App\Repositories;

use App\Models\Influencer;
use App\Repositories\BaseRepository;

/**
 * Class InfluencerRepository
 * @package App\Repositories
 * @version August 26, 2019, 5:35 am UTC
*/

class InfluencerRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'vendor_name',
        'vendor_image',
        'english_title',
        'arabic_title',
        'english_image',
        'arabic_image'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Influencer::class;
    }
}
