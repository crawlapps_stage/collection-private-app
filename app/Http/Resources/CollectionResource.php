<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class CollectionResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'handle' => $this->handle,
            'collection_image' => $this->collection_image,
            'lang_title' => $this->lang_title,
            'lang_image' => ($this->lang_image)?asset('storage/images/'.$this->lang_image):null,
        ];
    }
}
