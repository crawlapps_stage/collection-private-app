<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class InfluencerResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'vendor_name' => $this->vendor_name,
            'vendor_image' => ($this->vendor_image)?asset('storage/images/'.$this->vendor_image):null,
            'lang_title' => $this->lang_title,
            'lang_image' => ($this->lang_image)?asset('storage/images/'.$this->lang_image):null,
        ];
    }
}
