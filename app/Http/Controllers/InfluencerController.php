<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateInfluencerRequest;
use App\Http\Requests\UpdateInfluencerRequest;
use App\Repositories\InfluencerRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Response;
use App\Traits\ImageTrait;

class InfluencerController extends AppBaseController
{
    /** @var  InfluencerRepository */
    private $influencerRepository;

    use ImageTrait;

    public function __construct(InfluencerRepository $influencerRepo)
    {
        $this->influencerRepository = $influencerRepo;
    }

    /**
     * Display a listing of the Influencer.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $influencers = $this->influencerRepository->all();

        return view('influencers.index')
            ->with('influencers', $influencers);
    }

    /**
     * Show the form for creating a new Influencer.
     *
     * @return Response
     */
    public function create()
    {
        return view('influencers.create');
    }

    /**
     * Store a newly created Influencer in storage.
     *
     * @param CreateInfluencerRequest $request
     *
     * @return Response
     */
    public function store(CreateInfluencerRequest $request)
    {
        $input = $request->all();

        $files = $request->files->all();
        foreach ($files as $c_key => $c_value) {
            $path = "images/";
            $input[$c_key] = ImageTrait::makeImage($c_value, $path);
        }
        $influencer = $this->influencerRepository->create($input);

        Flash::success('Influencer saved successfully.');

        return redirect(route('influencers.index'));
    }

    /**
     * Display the specified Influencer.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $influencer = $this->influencerRepository->find($id);

        if (empty($influencer)) {
            Flash::error('Influencer not found');

            return redirect(route('influencers.index'));
        }

        return view('influencers.show')->with(['influencer'=> $influencer,'ImageTrait' => ImageTrait::class]);
    }

    /**
     * Show the form for editing the specified Influencer.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $influencer = $this->influencerRepository->find($id);

        if (empty($influencer)) {
            Flash::error('Influencer not found');

            return redirect(route('influencers.index'));
        }

        return view('influencers.edit')->with('influencer', $influencer);
    }

    /**
     * Update the specified Influencer in storage.
     *
     * @param int $id
     * @param UpdateInfluencerRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateInfluencerRequest $request)
    {
        $influencer = $this->influencerRepository->find($id);

        if (empty($influencer)) {
            Flash::error('Influencer not found');

            return redirect(route('influencers.index'));
        }
        $input = $request->all();
        $files = $request->files->all();
        foreach ($files as $c_key => $c_value) {
            $path = "images/";
            $input[$c_key] = ImageTrait::makeImage($c_value, $path);

        }
        $influencer = $this->influencerRepository->update($input, $id);

        Flash::success('Influencer updated successfully.');

        return redirect(route('influencers.index'));
    }

    /**
     * Remove the specified Influencer from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $influencer = $this->influencerRepository->find($id);

        if (empty($influencer)) {
            Flash::error('Influencer not found');

            return redirect(route('influencers.index'));
        }

        $this->influencerRepository->delete($id);

        Flash::success('Influencer deleted successfully.');

        return redirect(route('influencers.index'));
    }
}
