<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateCollectionRequest;
use App\Http\Requests\UpdateCollectionRequest;
use App\Repositories\CollectionRepository;
use App\Http\Controllers\AppBaseController;
use App\Traits\ImageTrait;
use Illuminate\Http\Request;
use Flash;
use Response;

class CollectionController extends AppBaseController
{
    /** @var  CollectionRepository */
    private $collectionRepository;

    public function __construct(CollectionRepository $collectionRepo)
    {
        \Log::info("__construct");
        $this->collectionRepository = $collectionRepo;
    }

    /**
     * Display a listing of the Collection.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $collections = $this->collectionRepository->all();

        return view('collections.index')
            ->with('collections', $collections);
    }

    /**
     * Show the form for creating a new Collection.
     *
     * @return Response
     */
    public function create()
    {
        abort(404);
        return view('collections.create');
    }

    /**
     * Store a newly created Collection in storage.
     *
     * @param CreateCollectionRequest $request
     *
     * @return Response
     */
    public function store(CreateCollectionRequest $request)
    {
        $input = $request->all();

        $files = $request->files->all();

        foreach ($files as $c_key => $c_value) {
            $path = "images/";
            $input[$c_key] = ImageTrait::makeImage($c_value, $path);
        }
        $collection = $this->collectionRepository->create($input);

        Flash::success('Collection saved successfully.');

        return redirect(route('collections.index'));
    }

    /**
     * Display the specified Collection.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $collection = $this->collectionRepository->find($id);

        if (empty($collection)) {
            Flash::error('Collection not found');

            return redirect(route('collections.index'));
        }

        return view('collections.show')->with('collection', $collection);
    }

    /**
     * Show the form for editing the specified Collection.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $collection = $this->collectionRepository->find($id);

        if (empty($collection)) {
            Flash::error('Collection not found');

            return redirect(route('collections.index'));
        }

        return view('collections.edit')->with('collection', $collection);
    }

    /**
     * Update the specified Collection in storage.
     *
     * @param int $id
     * @param UpdateCollectionRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateCollectionRequest $request)
    {
        $collection = $this->collectionRepository->find($id);

        if (empty($collection)) {
            Flash::error('Collection not found');

            return redirect(route('collections.index'));
        }
        $input = $request->all();
        $files = $request->files->all();
        foreach ($files as $c_key => $c_value) {
            $path = "images/";
            $input[$c_key] = ImageTrait::makeImage($c_value, $path);

        }
        $collection = $this->collectionRepository->update($input, $id);

        Flash::success('Collection updated successfully.');

        return redirect(route('collections.index'));
    }

    /**
     * Remove the specified Collection from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        abort(404);
        $collection = $this->collectionRepository->find($id);

        if (empty($collection)) {
            Flash::error('Collection not found');

            return redirect(route('collections.index'));
        }

        $this->collectionRepository->delete($id);

        Flash::success('Collection deleted successfully.');

        return redirect(route('collections.index'));
    }

    public function collection_create() {

        $hmac_header = $_SERVER['HTTP_X_SHOPIFY_HMAC_SHA256'];
        $data = file_get_contents('php://input');
        $verified = $this->verify_webhook($data, $hmac_header);
        $response_data=json_decode($data,1);

        $input['shopify_id'] = $response_data['id'];
        $input['name'] = $response_data['title'];
        $input['handle'] = $response_data['handle'];
        $input['collection_image'] = @$response_data['image']['src'];
        $collection = $this->collectionRepository->create($input);

    }

    public function collection_update() {
        \Log::info("collection_update");
        $hmac_header = $_SERVER['HTTP_X_SHOPIFY_HMAC_SHA256'];
        $data = file_get_contents('php://input');
        $verified = $this->verify_webhook($data, $hmac_header);
        $response_data=json_decode($data,1);

        $collection = $this->collectionRepository->model();
        $id = $collection::where('shopify_id',$response_data['id'])->firstOrFail()->id;
        $input['name'] = $response_data['title'];
        $input['handle'] = $response_data['handle'];
        $input['collection_image'] = @$response_data['image']['src'];
        $collection = $this->collectionRepository->update($input, $id);

    }
    public function collection_delete() {
        $hmac_header = $_SERVER['HTTP_X_SHOPIFY_HMAC_SHA256'];
        $data = file_get_contents('php://input');
        $verified = $this->verify_webhook($data, $hmac_header);
        $response_data=json_decode($data,1);

        $collection = $this->collectionRepository->model();
        $id = $collection::where('shopify_id',$response_data['id'])->delete();
    }


    public function verify_webhook($data, $hmac_header)
    {
        $calculated_hmac = base64_encode(hash_hmac('sha256', $data, env("WEBHOOK_KEY"), true));
        return hash_equals($hmac_header, $calculated_hmac);
    }
}
