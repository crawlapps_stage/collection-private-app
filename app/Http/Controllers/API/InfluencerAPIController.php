<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateInfluencerAPIRequest;
use App\Http\Requests\API\UpdateInfluencerAPIRequest;
use App\Models\Influencer;
use App\Repositories\InfluencerRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Response;
use App\Http\Resources\InfluencerResource;
/**
 * Class InfluencerController
 * @package App\Http\Controllers\API
 */

class InfluencerAPIController extends AppBaseController
{
    /** @var  InfluencerRepository */
    private $influencerRepository;

    public function __construct(InfluencerRepository $influencerRepo)
    {
        $this->influencerRepository = $influencerRepo;
    }

    /**
     * Display a listing of the Influencer.
     * GET|HEAD /influencers
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $columns = [];

        if($request->lang == 'en'){
            $columns = ['id','vendor_name','vendor_image','english_title as lang_title','english_image as lang_image'];
        }else{
            $columns = ['id','vendor_name','vendor_image','arabic_title as lang_title','arabic_image as lang_image'];
        }

        $influencers = $this->influencerRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit'),
            $columns
        );

        return $this->sendResponse(InfluencerResource::collection($influencers), 'Influencers retrieved successfully');
    }

    /**
     * Store a newly created Influencer in storage.
     * POST /influencers
     *
     * @param CreateInfluencerAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateInfluencerAPIRequest $request)
    {
        $input = $request->all();

        $influencer = $this->influencerRepository->create($input);

        return $this->sendResponse($influencer->toArray(), 'Influencer saved successfully');
    }

    /**
     * Display the specified Influencer.
     * GET|HEAD /influencers/{id}
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var Influencer $influencer */
        $influencer = $this->influencerRepository->find($id);

        if (empty($influencer)) {
            return $this->sendError('Influencer not found');
        }

        return $this->sendResponse($influencer->toArray(), 'Influencer retrieved successfully');
    }

    /**
     * Update the specified Influencer in storage.
     * PUT/PATCH /influencers/{id}
     *
     * @param int $id
     * @param UpdateInfluencerAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateInfluencerAPIRequest $request)
    {
        $input = $request->all();

        /** @var Influencer $influencer */
        $influencer = $this->influencerRepository->find($id);

        if (empty($influencer)) {
            return $this->sendError('Influencer not found');
        }

        $influencer = $this->influencerRepository->update($input, $id);

        return $this->sendResponse($influencer->toArray(), 'Influencer updated successfully');
    }

    /**
     * Remove the specified Influencer from storage.
     * DELETE /influencers/{id}
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var Influencer $influencer */
        $influencer = $this->influencerRepository->find($id);

        if (empty($influencer)) {
            return $this->sendError('Influencer not found');
        }

        $influencer->delete();

        return $this->sendResponse($id, 'Influencer deleted successfully');
    }
}
