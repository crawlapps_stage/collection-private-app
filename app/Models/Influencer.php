<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Influencer
 * @package App\Models
 * @version August 26, 2019, 5:35 am UTC
 *
 * @property string vendor_name
 * @property string vendor_image
 * @property string english_title
 * @property string arabic_title
 * @property string english_image
 * @property string arabic_image
 */
class Influencer extends Model
{
    use SoftDeletes;

    public $table = 'influencers';
    

    protected $dates = ['deleted_at'];


    public $fillable = [
        'vendor_name',
        'vendor_image',
        'english_title',
        'arabic_title',
        'english_image',
        'arabic_image'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'vendor_name' => 'string',
        'vendor_image' => 'string',
        'english_title' => 'string',
        'arabic_title' => 'string',
        'english_image' => 'string',
        'arabic_image' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'vendor_name' => 'required'
    ];

    
}
