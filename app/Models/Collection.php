<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Collection
 * @package App\Models
 * @version August 26, 2019, 7:10 am UTC
 *
 * @property string shopify_id
 * @property string name
 * @property string handle
 * @property string english_title
 * @property string arabic_title
 * @property string english_image
 * @property string arabic_image
 */
class Collection extends Model
{
    use SoftDeletes;

    public $table = 'collections';
    

    protected $dates = ['deleted_at'];


    public $fillable = [
        'shopify_id',
        'name',
        'handle',
        'english_title',
        'arabic_title',
        'english_image',
        'arabic_image'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'shopify_id' => 'string',
        'name' => 'string',
        'handle' => 'string',
        'english_title' => 'string',
        'arabic_title' => 'string',
        'english_image' => 'string',
        'arabic_image' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'shopify_id' => 'nullable',
        'name' => 'nullable',
        'handle' => 'nullable',
        'english_title' => 'nullable',
        'arabic_title' => 'nullable',
        'english_image' => 'nullable',
        'arabic_image' => 'nullable'
    ];

    
}
