<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Collection;
use Faker\Generator as Faker;

$factory->define(Collection::class, function (Faker $faker) {

    return [
        'shopify_id' => $faker->word,
        'name' => $faker->word,
        'handle' => $faker->word,
        'english_title' => $faker->word,
        'arabic_title' => $faker->word,
        'english_image' => $faker->word,
        'arabic_image' => $faker->word,
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s')
    ];
});
