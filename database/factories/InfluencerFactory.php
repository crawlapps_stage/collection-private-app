<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Influencer;
use Faker\Generator as Faker;

$factory->define(Influencer::class, function (Faker $faker) {

    return [
        'vendor_name' => $faker->word,
        'vendor_image' => $faker->word,
        'english_title' => $faker->word,
        'arabic_title' => $faker->word,
        'english_image' => $faker->word,
        'arabic_image' => $faker->word,
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s')
    ];
});
