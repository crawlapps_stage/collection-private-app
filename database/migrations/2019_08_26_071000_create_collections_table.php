<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateCollectionsTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('collections', function (Blueprint $table) {
            $table->increments('id');
            $table->string('shopify_id')->nullable();
            $table->string('name')->nullable();
            $table->string('handle')->nullable();
            $table->string('english_title')->nullable();
            $table->string('arabic_title')->nullable();
            $table->string('english_image')->nullable();
            $table->string('arabic_image')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('collections');
    }
}
