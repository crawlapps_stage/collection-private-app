<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;
use App\Models\Influencer;

class InfluencerApiTest extends TestCase
{
    use ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_influencer()
    {
        $influencer = factory(Influencer::class)->make()->toArray();

        $this->response = $this->json(
            'POST',
            '/api/influencers', $influencer
        );

        $this->assertApiResponse($influencer);
    }

    /**
     * @test
     */
    public function test_read_influencer()
    {
        $influencer = factory(Influencer::class)->create();

        $this->response = $this->json(
            'GET',
            '/api/influencers/'.$influencer->id
        );

        $this->assertApiResponse($influencer->toArray());
    }

    /**
     * @test
     */
    public function test_update_influencer()
    {
        $influencer = factory(Influencer::class)->create();
        $editedInfluencer = factory(Influencer::class)->make()->toArray();

        $this->response = $this->json(
            'PUT',
            '/api/influencers/'.$influencer->id,
            $editedInfluencer
        );

        $this->assertApiResponse($editedInfluencer);
    }

    /**
     * @test
     */
    public function test_delete_influencer()
    {
        $influencer = factory(Influencer::class)->create();

        $this->response = $this->json(
            'DELETE',
             '/api/influencers/'.$influencer->id
         );

        $this->assertApiSuccess();
        $this->response = $this->json(
            'GET',
            '/api/influencers/'.$influencer->id
        );

        $this->response->assertStatus(404);
    }
}
