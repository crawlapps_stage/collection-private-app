<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;
use App\Models\Collection;

class CollectionApiTest extends TestCase
{
    use ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_collection()
    {
        $collection = factory(Collection::class)->make()->toArray();

        $this->response = $this->json(
            'POST',
            '/api/collections', $collection
        );

        $this->assertApiResponse($collection);
    }

    /**
     * @test
     */
    public function test_read_collection()
    {
        $collection = factory(Collection::class)->create();

        $this->response = $this->json(
            'GET',
            '/api/collections/'.$collection->id
        );

        $this->assertApiResponse($collection->toArray());
    }

    /**
     * @test
     */
    public function test_update_collection()
    {
        $collection = factory(Collection::class)->create();
        $editedCollection = factory(Collection::class)->make()->toArray();

        $this->response = $this->json(
            'PUT',
            '/api/collections/'.$collection->id,
            $editedCollection
        );

        $this->assertApiResponse($editedCollection);
    }

    /**
     * @test
     */
    public function test_delete_collection()
    {
        $collection = factory(Collection::class)->create();

        $this->response = $this->json(
            'DELETE',
             '/api/collections/'.$collection->id
         );

        $this->assertApiSuccess();
        $this->response = $this->json(
            'GET',
            '/api/collections/'.$collection->id
        );

        $this->response->assertStatus(404);
    }
}
