<?php namespace Tests\Repositories;

use App\Models\Influencer;
use App\Repositories\InfluencerRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;

class InfluencerRepositoryTest extends TestCase
{
    use ApiTestTrait, DatabaseTransactions;

    /**
     * @var InfluencerRepository
     */
    protected $influencerRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->influencerRepo = \App::make(InfluencerRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_influencer()
    {
        $influencer = factory(Influencer::class)->make()->toArray();

        $createdInfluencer = $this->influencerRepo->create($influencer);

        $createdInfluencer = $createdInfluencer->toArray();
        $this->assertArrayHasKey('id', $createdInfluencer);
        $this->assertNotNull($createdInfluencer['id'], 'Created Influencer must have id specified');
        $this->assertNotNull(Influencer::find($createdInfluencer['id']), 'Influencer with given id must be in DB');
        $this->assertModelData($influencer, $createdInfluencer);
    }

    /**
     * @test read
     */
    public function test_read_influencer()
    {
        $influencer = factory(Influencer::class)->create();

        $dbInfluencer = $this->influencerRepo->find($influencer->id);

        $dbInfluencer = $dbInfluencer->toArray();
        $this->assertModelData($influencer->toArray(), $dbInfluencer);
    }

    /**
     * @test update
     */
    public function test_update_influencer()
    {
        $influencer = factory(Influencer::class)->create();
        $fakeInfluencer = factory(Influencer::class)->make()->toArray();

        $updatedInfluencer = $this->influencerRepo->update($fakeInfluencer, $influencer->id);

        $this->assertModelData($fakeInfluencer, $updatedInfluencer->toArray());
        $dbInfluencer = $this->influencerRepo->find($influencer->id);
        $this->assertModelData($fakeInfluencer, $dbInfluencer->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_influencer()
    {
        $influencer = factory(Influencer::class)->create();

        $resp = $this->influencerRepo->delete($influencer->id);

        $this->assertTrue($resp);
        $this->assertNull(Influencer::find($influencer->id), 'Influencer should not exist in DB');
    }
}
