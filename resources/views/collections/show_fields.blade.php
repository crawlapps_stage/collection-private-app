<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $collection->id !!}</p>
</div>

<!-- Shopify Id Field -->
<div class="form-group">
    {!! Form::label('shopify_id', 'Shopify Id:') !!}
    <p>{!! $collection->shopify_id !!}</p>
</div>

<!-- Name Field -->
<div class="form-group">
    {!! Form::label('name', 'Name:') !!}
    <p>{!! $collection->name !!}</p>
</div>

<!-- Handle Field -->
<div class="form-group">
    {!! Form::label('handle', 'Handle:') !!}
    <p>{!! $collection->handle !!}</p>
</div>

<!-- Collection Image Field -->
<div class="form-group">
    {!! Form::label('collection_image', 'Collection Image:') !!}
    @if($collection->collection_image)
        <img width="100" src="{{ $collection->collection_image }}" />
    @endif
</div>

<!-- English Title Field -->
<div class="form-group">
    {!! Form::label('english_title', 'English Title:') !!}
    <p>{!! $collection->english_title !!}</p>
</div>

<!-- Arabic Title Field -->
<div class="form-group">
    {!! Form::label('arabic_title', 'Arabic Title:') !!}
    <p>{!! $collection->arabic_title !!}</p>
</div>

<!-- English Image Field -->
<div class="form-group">
    {!! Form::label('english_image', 'English Image:') !!}
    @if($collection->english_image)
    <img width="100" src="{{ asset('storage/images/'.$collection->english_image) }}" />
    @endif
</div>

<!-- Arabic Image Field -->
<div class="form-group">
    {!! Form::label('arabic_image', 'Arabic Image:') !!}
    @if($collection->arabic_image)
    <img width="100" src="{{ asset('storage/images/'.$collection->arabic_image) }}" />
    @endif
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{!! $collection->created_at !!}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{!! $collection->updated_at !!}</p>
</div>

