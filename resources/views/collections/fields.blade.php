<!-- Name Field -->
<div class="form-group col-sm-6">
    {!! Form::label('name', 'Shopify Name:') !!}
    {!! Form::text('name', null, ['class' => 'form-control','readonly'=>true]) !!}
</div>

<!-- English Title Field -->
<div class="form-group col-sm-6">
    {!! Form::label('english_title', 'English Title:') !!}
    {!! Form::text('english_title', null, ['class' => 'form-control']) !!}
</div>

<!-- Arabic Title Field -->
<div class="form-group col-sm-6">
    {!! Form::label('arabic_title', 'Arabic Title:') !!}
    {!! Form::text('arabic_title', null, ['class' => 'form-control']) !!}
</div>

<!-- English Image Field -->
<div class="form-group col-sm-6">
    {!! Form::label('english_image', 'English Image:') !!}
    {!! Form::file('english_image') !!}
</div>
<div class="clearfix"></div>

<!-- Arabic Image Field -->
<div class="form-group col-sm-6">
    {!! Form::label('arabic_image', 'Arabic Image:') !!}
    {!! Form::file('arabic_image') !!}
</div>
<div class="clearfix"></div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('collections.index') !!}" class="btn btn-default">Cancel</a>
</div>
