<div class="table-responsive">
    <table class="table" id="collections-table">
        <thead>
            <tr>
                <th>Name</th>
        <th>English Title</th>
        <th>Arabic Title</th>
        <th>English Image</th>
        <th>Arabic Image</th>
                <th colspan="3">Action</th>
            </tr>
        </thead>
        <tbody>
        @foreach($collections as $collection)
            <tr>
                <td>{!! $collection->name !!}</td>
            <td>{!! $collection->english_title !!}</td>
            <td>{!! $collection->arabic_title !!}</td>
            <td>{!! $collection->english_image !!}</td>
            <td>{!! $collection->arabic_image !!}</td>
                <td>
                    <div class='btn-group'>
                        <a href="{!! route('collections.show', [$collection->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                        <a href="{!! route('collections.edit', [$collection->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>

                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
