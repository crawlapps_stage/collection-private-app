<div class="table-responsive">
    <table class="table" id="influencers-table">
        <thead>
            <tr>
                <th>Vendor Name</th>
        <th>Vendor Image</th>
        <th>English Title</th>
        <th>Arabic Title</th>
        <th>English Image</th>
        <th>Arabic Image</th>
                <th colspan="3">Action</th>
            </tr>
        </thead>
        <tbody>
        @foreach($influencers as $influencer)
            <tr>
                <td>{!! $influencer->vendor_name !!}</td>
            <td>{!! $influencer->vendor_image !!}</td>
            <td>{!! $influencer->english_title !!}</td>
            <td>{!! $influencer->arabic_title !!}</td>
            <td>{!! $influencer->english_image !!}</td>
            <td>{!! $influencer->arabic_image !!}</td>
                <td>
                    {!! Form::open(['route' => ['influencers.destroy', $influencer->id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        <a href="{!! route('influencers.show', [$influencer->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                        <a href="{!! route('influencers.edit', [$influencer->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                        {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
