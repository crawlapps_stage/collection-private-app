<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $influencer->id !!}</p>
</div>

<!-- Vendor Name Field -->
<div class="form-group">
    {!! Form::label('vendor_name', 'Vendor Name:') !!}
    <p>{!! $influencer->vendor_name !!}</p>
</div>

<!-- Vendor Image Field -->
<div class="form-group">
    {!! Form::label('vendor_image', 'Vendor Image:') !!}
    @if($influencer->vendor_image)
    <img width="100" src="{{ asset('storage/images/'.$influencer->vendor_image) }}" />
    @endif
</div>

<!-- English Title Field -->
<div class="form-group">
    {!! Form::label('english_title', 'English Title:') !!}
    <p>{!! $influencer->english_title !!}</p>
</div>

<!-- Arabic Title Field -->
<div class="form-group">
    {!! Form::label('arabic_title', 'Arabic Title:') !!}
    <p>{!! $influencer->arabic_title !!}</p>
</div>

<!-- English Image Field -->
<div class="form-group">
    {!! Form::label('english_image', 'English Image:') !!}
    @if($influencer->english_image)
    <img width="100" src="{{ asset('storage/images/'.$influencer->english_image) }}" />
    @endif
</div>

<!-- Arabic Image Field -->
<div class="form-group">
    {!! Form::label('arabic_image', 'Arabic Image:') !!}
    @if($influencer->arabic_image)
    <img width="100" src="{{ asset('storage/images/'.$influencer->arabic_image) }}" />
     @endif
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{!! $influencer->created_at !!}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{!! $influencer->updated_at !!}</p>
</div>

